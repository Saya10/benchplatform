## Build images

make postgres-build 
make mongo-build
make client-build
make postgres-edb-build

## Finding source images

```
python /usr/local/bin/aws ec2 describe-images --filters "Name=name,Values=ubuntu-bionic*"
```