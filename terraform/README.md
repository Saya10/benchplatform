
## Building Data Load

The file name convention in the `dataLoad` folder will be: `<database>-<tool>-<fs>-<size>`

Each file will contain an instance and a EBS disk, under its corresponding format. A post-script for loading the initial data
will be triggered.

```
resource "aws_ebs_volume" "postgres-sysb-xfs-fit" {
  availability_zone = "sa-east-1a"
  size              = 40

  tags = {
    Name = "postgres-sysb-xfs-fit"
  }
}

```


## Making Snapshots


```
resource "aws_ebs_snapshot" "postgres-sysb-xfs-fi-snap" {
  volume_id = "${aws_ebs_volume.postgres-sysb-xfs-fit.id}"

  tags = {
    Name = "postgres-sysb-xfs-fit"
  }
}
```

## Run Benchmark
