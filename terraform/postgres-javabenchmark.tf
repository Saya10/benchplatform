/*
This module is pregenerated by `make prepare`. 
*/



data "yaml_map_of_strings" "config-postgres-javabenchmark" {
   input = "${data.yaml_map_of_strings.config.output["postgres-javabenchmark"]}" 
}



module "postgres-javabenchmark" {

    source = "modules/compute_set"

    # DEPRECATED!! XXX:
    #subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"

    subnet_id                   = "${aws_subnet.main.id}"
    vpc_security_group_ids      = ["${module.security_group.this_security_group_id}"]
    associate_public_ip_address = true

    key_name                    = "${aws_key_pair.benchplatform.key_name}"

    iam_instance_profile        = "${aws_iam_instance_profile.generic-profile.name}"

    globalconfig                = "${data.yaml_map_of_strings.config-global.output}"
    config                      = "${data.yaml_map_of_strings.config-postgres-javabenchmark.output}"

    target                      = "postgres-javabenchmark"       
    s3uri                       = "${aws_s3_bucket.results-bucket.bucket_regional_domain_name}"
    sqsQueue                    = "${aws_sqs_queue.status_queue.id}"
}



output "postgres-javabenchmark-clientip" {
  value = "${module.postgres-javabenchmark.client_public_ip}"
}

# XXX: Make a map of strings of both ip listing, withint the name of the datanodes .
# See inside compute_set/datanode.tf

output "postgres-javabenchmark-datanodeloadips" {
  value = "${module.postgres-javabenchmark.datanode_load_public_ip}"
}

output "postgres-javabenchmark-datanoderunips" {
  value = "${module.postgres-javabenchmark.datanode_run_public_ip}"
}

output "postgres-javabenchmark-resultsUrl" {
  value = "${module.postgres-javabenchmark.resultsUrl}"
}