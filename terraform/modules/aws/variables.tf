variable "project" {
    default = "benchplatform"
    description = "Project Name (used widely)"
}

variable "configfile" {
    default = "${module.path}/etc/config.yaml"
} 

variable "region" {

    default = "us-east-1"
}

variable "s3pathsuffix" {
    default = "-results"
}

