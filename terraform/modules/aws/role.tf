
resource "aws_iam_role" "generic-role" {
  path = "/"
  name = "generic-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "s3.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}


resource "aws_iam_instance_profile" "generic-profile" {
  name = "generic-profile"
  role = "${aws_iam_role.generic-role.name}"
}

resource "aws_iam_policy" "ec2policy" {
  name        = "ec2policy"
  path        = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ec2fullattach" {
    role = "${aws_iam_role.generic-role.name}"
    policy_arn = "${aws_iam_policy.ec2policy.arn}"
}

resource "aws_iam_policy" "sqspolicy" {
  name        = "sqspolicy"
  path        = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sqs:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "sqsfullattach" {
    role = "${aws_iam_role.generic-role.name}"
    policy_arn = "${aws_iam_policy.sqspolicy.arn}"
}


resource "aws_iam_policy" "s3policy" {
  name        = "s3policy"
  path        = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s3fullattach" {
    role = "${aws_iam_role.generic-role.name}"
    policy_arn = "${aws_iam_policy.s3policy.arn}"
}

