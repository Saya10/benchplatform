# TODO

We are going to use the following convention for creating modules:

<provider>_<shortdesc>

eg. for aws, would be "aws_datanode_client_set" 

Each module should be accompanied with its terraform/templates/<modulename>.template


`plain.template` will still remain as it is intended to mount harcoded 
infrastructure.


## Refactor of actions

We may want to have different modules for the actions, so we don't need to do too many 
logic in the module. That is, if you want to spin a snapshot, using template/aws_snapshot_compute_set,
it will run the corresponding snapshot for the specified target.

So, for all the actions, you need to build the module from the template, but using the action name as the
template selector, allowing things like:

```
make TARGET="target" PROVIDER="provider" <action>|<install>
```

`install` does apply the base directory 