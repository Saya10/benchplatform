
resource "aws_instance" "datanode-load" {
  ami                         = "${data.aws_ami.datanode_image.image_id}"    #"${data.aws_ami.datanode_image}" # us-west-2
  instance_type               = "${var.config["datanode_instance_type"]}"
  count                       = "${var.config["action"] == "load" ? var.config["factor"] : 0}"
  associate_public_ip_address = "${var.associate_public_ip_address}"
  subnet_id                   = "${var.subnet_id}"
  vpc_security_group_ids      = ["${var.vpc_security_group_ids}"]
  key_name                    = "${var.key_name}"
  iam_instance_profile        = "${var.iam_instance_profile}"
  # availability_zone           = "${var.globalconfig["region"]}a"

  # ebs_optimized               = "${var.config["datanode_instance_type"] == "t2.small" ? false : true }"

  user_data_base64          = "${data.template_cloudinit_config.datanode_config.rendered}"

  ebs_block_device {
      device_name           = "${var.config["device_name"]}"
      volume_size           = "${var.config["disk_size"]}"
      volume_type           = "${var.config["ebs_type"]}"
      delete_on_termination = true
      iops                  = "${var.config["disk_iops"]}"
  }
  credit_specification {
    cpu_credits               = "unlimited"
  }

  tags = "${merge(map("Name", format("datanode-%s-%d", var.target, count.index+1),
                     "Dbtagname", format("datanode-%s-%d", var.target, count.index+1),
                     "Benchid", "${random_uuid.benchid.result}"
                     ), local.pretags)}"
                      # self reference not allowed
                      #,"Volumeid", "${aws_instance.datanode-load.ebs_block_device.0.volume_id}"
  provisioner "remote-exec" {
    inline = [
      "mkdir /home/${var.globalconfig["sshuser"]}/conf",
    ]

    connection {
      type          = "ssh"
      user          = "${var.globalconfig["sshuser"]}" 
      private_key   =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}" 
    }
  }
  provisioner "file" {
    source      = "${path.module}/conf"
    destination = "/home/${var.globalconfig["sshuser"]}"

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    }
  }
  provisioner "file" {
    source      = "~/.ssh/id_rsa_${var.globalconfig["project"]}"
    destination = "/home/${var.globalconfig["sshuser"]}/.ssh/id_rsa"

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    }
  }

  provisioner "file" {
    source      = "~/.ssh/id_rsa_${var.globalconfig["project"]}.pub"
    destination = "/home/${var.globalconfig["sshuser"]}/.ssh/id_rsa.pub"

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    }
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 600 /home/${var.globalconfig["sshuser"]}/.ssh/*",
    ]

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    }
  }

  lifecycle {
    # Due to several known issues in Terraform AWS provider related to arguments of aws_instance:
    # (eg, https://github.com/terraform-providers/terraform-provider-aws/issues/2036)
    # we have to ignore changes in the following arguments
    ignore_changes = ["private_ip", "root_block_device", "ebs_block_device"]
  }
}

/*
Notes about run differences:

- snapshot id is used
- timeout is set to  "45m" due that snapshot takes time to spin up 

*/
resource "aws_instance" "datanode-run" {
  ami                         = "${data.aws_ami.datanode_image.image_id}"    #"${data.aws_ami.datanode_image}" # us-west-2
  instance_type               = "${var.config["datanode_instance_type"]}"
  count                       = "${var.config["action"] == "run" ? var.config["factor"] : 0}"
  associate_public_ip_address = "${var.associate_public_ip_address}"
  subnet_id                   = "${var.subnet_id}"
  vpc_security_group_ids      = ["${var.vpc_security_group_ids}"]
  iam_instance_profile        = "${var.iam_instance_profile}"
  key_name                    = "${var.key_name}"
  # availability_zone           = "${var.globalconfig["region"]}a"

  # ebs_optimized               = "${var.config["datanode_instance_type"] == "t2.small" ? false : true }"

  user_data_base64          = "${data.template_cloudinit_config.datanode_config.rendered}"

  ebs_block_device {
      device_name           = "${var.config["device_name"]}"
      volume_size           = "${var.config["disk_size"]}"
      volume_type           = "${var.config["ebs_type"]}"
      delete_on_termination = true
      iops                  = "${var.config["disk_iops"]}"
      snapshot_id           = "${data.aws_ebs_snapshot.ebs_volume.id}"
  }

  credit_specification {
    cpu_credits               = "unlimited"
  }

  tags = "${merge(map("Name", format("datanode-%s-%d", var.target, count.index+1),
                      "Dbtagname", format("datanode-%s-%d", var.target, count.index+1),
                      "Benchid", "${random_uuid.benchid.result}"
                     ), local.pretags)}"

  # , "Volumeid", "${aws_instance.datanode-run.ebs_block_device.0.volume_id}"
  # self reference not allowed 

  # https://www.terraform.io/docs/provisioners/file.html
  # trailing slash handling is important here. We want to keep the code agnostic
  # as possible, but configuration can vary considerably between each engine.
  # So, we copy the entire folder to /opt/conf and we let the custom scripts to 
  # handle this.
  # 
  provisioner "remote-exec" {
    inline = [
      "mkdir /home/${var.globalconfig["sshuser"]}/conf",
    ]

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
      timeout       = "45m"
    }
  }
  provisioner "file" {
    source      = "${path.module}/conf"
    destination = "/home/${var.globalconfig["sshuser"]}"

    connection {
      type        = "ssh"
      user        = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
      timeout       = "45m"
    }
  }

  provisioner "file" {
    content     = "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    destination = "/home/${var.globalconfig["sshuser"]}/.ssh/id_rsa"

    connection {
      type        = "ssh"
      user        = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
      timeout       = "45m"
    }
  }

  provisioner "file" {
    content     =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}.pub")}"
    destination = "/home/${var.globalconfig["sshuser"]}/.ssh/id_rsa.pub"

    connection {
      type        = "ssh"
      user        = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
      timeout       = "45m"
    }
  }
  
  lifecycle {
    # Due to several known issues in Terraform AWS provider related to arguments of aws_instance:
    # (eg, https://github.com/terraform-providers/terraform-provider-aws/issues/2036)
    # we have to ignore changes in the following arguments
    ignore_changes = ["private_ip", "root_block_device", "ebs_block_device"]
  }
}


# Another way to attach volumes (might be more factorizable than automatic)

# resource "aws_volume_attachment" "volume_attach_load" {
#   count       = "${var.config["action"] == "load" ? var.config["factor"] : 0}" 
#   device_name = "${var.config["device_name"]}" 
#   volume_id   = "${aws_ebs_volume.volume_load.id[count.index]}"
#   instance_id = "${aws_instance.datanode-load.id[count.index]}"

# }

# resource "aws_ebs_volume" "volume_load" {
#   count             = "${var.config["action"] == "load" ? var.config["factor"] : 0}" 
# #   availability_zone = "${module.datanode.availability_zone[0]}"
#   availability_zone = "${aws_instance.datanode-load.availability_zone}"
#   size              = "${var.config["disk_size"]}"
#   iops              = "${var.config["disk_iops"]}" # FIXME!
#   # This needs to be conditional, so when initialLoad is the action
#   # snapshot_id must be different (probably 2 volumes and if?)
#   # snapshot_id = "mongo_data" # posgres_data
#   type        = "${var.config["ebs_type"]}"  

#   tags = {
#     Name = "${var.target}"
#   }
# }



# resource "aws_volume_attachment" "volume_attach_run" {
#   count             = "${var.config["action"] == "run" ? var.config["factor"] : 0}" 

#   device_name = "${var.config["device_name"]}"
#   volume_id   = "${aws_ebs_volume.volume_run.id[count.index]}"
#   instance_id = "${aws_instance.datanode-run.id[count.index]}"

# }

# resource "aws_ebs_volume" "volume_run" {
#   count             = "${var.config["action"] == "run" ? var.config["factor"] : 0}" 
#   availability_zone = "${aws_instance.datanode-run.availability_zone}"
#   size              = "${var.config["disk_size"]}"
#   iops              = "${var.config["disk_iops"]}" # FIXME!
#   # This needs to be conditional, so when initialLoad is the action
#   # snapshot_id must be different (probably 2 volumes and if?)
#   snapshot_id       = "${data.aws_ebs_snapshot.ebs_volume.id}" # posgres_data
#   type              = "${var.config["ebs_type"]}"  
#   tags = {
#     Name = "${var.target}"
#   }
# }

