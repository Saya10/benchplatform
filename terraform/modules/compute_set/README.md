## How you call this module

The module implementation in this reposity is done through the root Makefile setup.

A final version might look as the bellow:

```
data "yaml_map_of_strings" "config-snapshot-postgres-sysbench-xfs-fit" {
   input = "${data.yaml_map_of_strings.config.output["snapshot-postgres-sysbench-xfs-fit"]}" 
}



module "snapshot-postgres-sysbench-xfs-fit" {

    source = "modules/compute_set"

    subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
    vpc_security_group_ids      = ["${module.security_group.this_security_group_id}"]
    associate_public_ip_address = true

    key_name                    = "${aws_key_pair.benchplatform.key_name}"

    iam_instance_profile        = "${aws_iam_instance_profile.generic-profile.name}"

    globalconfig                = "${data.yaml_map_of_strings.config-global.output}"
    config                      = "${data.yaml_map_of_strings.config-snapshot-postgres-sysbench-xfs-fit.output}"

    target                      = "snapshot-postgres-sysbench-xfs-fit"
    s3uri                       = "${aws_s3_bucket.results-bucket.bucket_regional_domain_name}"
}



output "snapshot-postgres-sysbench-xfs-fit-clientip" {
  value = "${module.snapshot-postgres-sysbench-xfs-fit.client_public_ip}"
}

output "snapshot-postgres-sysbench-xfs-fit-datanodeloadip" {
  value = "${module.snapshot-postgres-sysbench-xfs-fit.datanode_load_public_ip}"
}

output "snapshot-postgres-sysbench-xfs-fit-datanoderunip" {
  value = "${module.snapshot-postgres-sysbench-xfs-fit.datanode_run_public_ip}"
}
```


## Folders

- `conf`:  all configuration files, organized by engine
- `files`: executable files for each action (load and run in this case)
- `lib`: general executable scripts and functions. defatult.sh contains useful functions
- `templates`: cloud-init templates mostly, but can be placed here all the files that require configuration.


## Information 

### Tagging volumes at entrypoint.sh

ec2-create-tags $(getVolumeId) -t Name=$(getTag Target)
