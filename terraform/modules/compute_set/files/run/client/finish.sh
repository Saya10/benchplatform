#!/usr/bin/env bash

source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"

echo "Start finish script"


# Stop local sadc and iostat
stopSadc
stopIostat
remote-datanode-exec "sudo pkill sadc"
remote-datanode-exec "sudo pkill iostat"

remote-datanode-exec "df -h" > ${RESULTS_DIR}/finish_datanode_df.json


for file in $(ls ${RESULTS_DIR}/*out )
do
    generateCSV ${file}
done

# Upload local files
uploadLocalResults
remote-datanode-exec 'source /usr/local/bin/default.sh && uploadLocalResults'
echo "End finish script"
