#!/usr/bin/env bash

source /usr/local/bin/default.sh


dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
threads=$(getTag Threads)
step=$(getTag Step)
dbengine=$(getTag Dbengine)
RESULTS_DIR="$(getTag Resultsdir)"


## Clone repo

cd /opt 
git clone https://gitlab.com/ongresinc/txbenchmark.git

cd txbenchmark
mvn clean package

## Generate SQL 
mkdir resources
wget -P resources http://www.lsv.fr/~sirangel/teaching/dataset/aircrafts.txt
wget -P resources http://www.lsv.fr/~sirangel/teaching/dataset/schedule.txt

## For PG
psql -h$dbhost -Upostgres $postgres -c "create database $dbname"
psql -h$dbhost -Upostgres $dbname -c "drop table if exists audit"
psql -h$dbhost -Upostgres $dbname -c "drop table if exists payment"
psql -h$dbhost -Upostgres $dbname -c "drop table if exists seat"
psql -h$dbhost -Upostgres $dbname -c "drop table if exists schedule"
psql -h$dbhost -Upostgres $dbname -c "drop table if exists aircraft"
psql -h$dbhost -Upostgres $dbname -c "create extension if not exists \"uuid-ossp\""
psql -h$dbhost -Upostgres $dbname -c "create table aircraft (name text, icao text, iata text, capacity integer, country text)"
psql -h$dbhost -Upostgres $dbname -c "create table schedule (from_airport text, to_airport text, valid_from text, valid_until text, days integer, departure text, arrival text, flight text, aircraft text, duration text)"
psql -h$dbhost -Upostgres $dbname -c "create table seat (user_id bigint not null, schedule_id uuid not null, day date not null, date timestamp without time zone, primary key (user_id,schedule_id,day))"
psql -h$dbhost -Upostgres $dbname -c "create table payment (user_id bigint, amount money, date timestamp without time zone)"
psql -h$dbhost -Upostgres $dbname -c "create table audit (schedule_id uuid not null, day date not null, date timestamp without time zone, seats_occupied int, primary key (schedule_id,day))"
psql -h$dbhost -Upostgres $dbname -c "\\copy aircraft from 'resources/aircrafts.txt' with csv header delimiter ';' null '\\N'"
psql -h$dbhost -Upostgres $dbname -c "\\copy schedule from 'resources/schedule.txt' with csv header delimiter ';' null '\\N'"
psql -h$dbhost -Upostgres $dbname -c "alter table schedule add column schedule_id uuid primary key default uuid_generate_v4()"
psql -h$dbhost -Upostgres $dbname -c "alter table seat add foreign key (schedule_id) references schedule(schedule_id)"
psql -h$dbhost -Upostgres $dbname -c "alter table audit add foreign key (schedule_id) references schedule(schedule_id)"



for thread_iter in $(seq  ${threads} -${step} 1 | head -${cycles} )
do
  echo "engine:$dbengine parallel:$thread_iter host:$dbhost duration:$duration"
  java -jar cli/target/benchmark-1.3.jar \
               --benchmark-target $dbengine \
               --parallelism $thread_iter \
               --day-range 1 \
               --booking-sleep 0 \
               --target-database-host $dbhost \
               --target-database-port 5432 \
               --target-database-user "postgres" \
               --target-database-password "1234" \
               --target-database-name $dbname \
               --metrics "PT1S" \
               --metrics-reporter csv --duration "PT${duration}S" \
               --max-connections 700

    
    
  ## After execution, we need to clean some data to avoid errors
    psql -h$dbhost -Upostgres $dbname -c "truncate audit cascade"
    psql -h$dbhost -Upostgres $dbname -c "truncate payment cascade"
    psql -h$dbhost -Upostgres $dbname -c "truncate seat cascade"
    
    mv iterations.csv "${RESULTS_DIR}/iterations-${thread_iter}.csv"
    mv response-time.csv "${RESULTS_DIR}/response-time-${thread_iter}.csv"
    mv retry.csv "${RESULTS_DIR}/retry-${thread_iter}.csv"

    sleep 5
done