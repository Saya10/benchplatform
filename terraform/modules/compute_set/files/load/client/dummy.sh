source /usr/local/bin/default.sh

# This script allows to the load process with no action.
# you can run and build your own thing.


tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
threads=$(getTag Threads)
step=$(getTag Step)
RESULTS_DIR="$(getTag Resultsdir)"


# This just gets the status of the latest snapshot and keeps things
# in a loop to avoid finish script to execute.
while true
do
    snapstatus=$(getSnapshotStatus)
    echo "Snapshot Status: ${snapstatus}" 
    if [ "x${snapstatus}" == "xcompleted" ]; then
        exit
    fi 
    sleep 10 
done