#!/usr/bin/env bash

source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
step=$(getTag Step)
target=$(getTag Target)
datestr=$(date)
action=$(getTag Action)
client=$(getTag Name)
dbengine=$(getTag Dbengine)
tool=$(getTag Tool)
fs=$(getTag Fs)

makeBucket

startSadc
startIostat

# see lib/default.sh, this works for all engines
until is_db_up
  do sleep 2 
done

echo "Entrypoint finished"

cat <<EOF > ${RESULTS_DIR}/metadata.json
{
  "target": "${target}",
  "fs": "${fs}",
  "tool": "${tool}",
  "dbengine": "${dbengine}",
  "tables": "${tables}",
  "numrows": "${numrows}",
  "threads": "${threads}",
  "cycles": "${cycles}",
  "duration": "${duration}",
  "step": "${step}",
  "action": "${action}",
  "start_date": "${datestr}",
  "datanode": "${dbhost}",
  "client": "${client}"
}
EOF