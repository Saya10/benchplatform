#!/usr/bin/env bash

#WIP
# TODO
# replace the range time with a variable.
# desired e.g:
# rtime="{08..12}-{01..31}-{0..23}" or rtime=${getTag Rtime}
# for file in http://data.gharchive.org/2015-${rtime}.json.gz ; do
# Could add new parameters to give range time?

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
tablename="github2015"
RESULTS_DIR="$(getTag Resultsdir)"

# Init
echo START: `date +'%F_%T'`

/usr/bin/psql -h ${dbhost} -Upostgres -c "CREATE DATABASE ${dbname}" 
/usr/bin/psql -h${dbhost} -Upostgres -d${dbname} -c "CREATE TABLE ${tablename} ( event_id serial NOT NULL, data jsonb)" 

for file in http://data.gharchive.org/2015-{01..12}-{01..31}-{0..23}.json.gz
    do
        wget -q -o /dev/null -O - $file | gunzip -c | sed 's/\\u0000/\\u0001/g' | psql -Upostgres -d${dbname} -h${dbhost} -c "COPY ${tablename} (data) FROM STDIN csv quote e'\x01' delimiter e'\x02'"
    done

/usr/bin/psql -h${dbhost} -Upostgres -d${dbname} -c "CREATE INDEX CONCURRENTLY ON ${tablename} ((data->>'type'))"
/usr/bin/psql -h${dbhost} -Upostgres -d${dbname} -c "CREATE INDEX CONCURRENTLY ON ${tablename} ((data->'repo'->>'name'))"
/usr/bin/psql -h${dbhost} -Upostgres -d${dbname} -c "CREATE INDEX CONCURRENTLY ON ${tablename} ((data->'payload'->>'action'))"
/usr/bin/psql -h${dbhost} -Upostgres -d${dbname} -c "CREATE INDEX CONCURRENTLY ON ${tablename} ((data->'actor'->>'login'))"
/usr/bin/psql -h${dbhost} -Upostgres -d${dbname} -c "CREATE INDEX CONCURRENTLY ON ${tablename} ((data->'payload'->'issue'->'comments'))"

makeSnapshot

echo FINISH: `date +'%F_%T'`