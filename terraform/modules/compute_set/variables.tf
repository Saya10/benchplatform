# This variable is generated in a previously existing YAML
# This is just a map of strings
variable "config" {
    default = {}
}

variable "globalconfig" {
    default = {}
}

variable "target" {}
variable "s3uri" {}

variable "sqsQueue" {}

variable "subnet_id" {}
variable "vpc_security_group_ids" {
    default = []
}
variable "key_name" {}
variable "iam_instance_profile" {}
variable "associate_public_ip_address" {}

variable "disk_iops" {
    default = 1000
}

variable "tags" {
    default = {}
}

# Defaults
variable "defcycles" {
    default = 1
}

variable "defduration" {
    default = 300
}

variable "results_dir" {
    default = "/var/results"
}

variable "default_datadir" {
    default = "/opt/data"
}

locals {
    pretags = {
        Project     = "${var.globalconfig["project"]}"
        Sshuser     = "${lookup(var.globalconfig, "sshuser", "ubuntu")}"
        Action      = "${var.config["action"]}"
        Fs          = "${var.config["fs"]}"
        Target      = "${var.target}"
        Tool        = "${var.config["tool"]}"
        Loadsize    = "${var.config["loadsize"]}"
        Dbengine    = "${var.config["dbengine"]}"
        Dbname      = "${var.config["dbname"]}"
        Devicename  = "${var.config["device_name"]}"
        S3uri       = "${var.s3uri}"
        Sqsqueue    = "${var.sqsQueue}"
        Numrows     = "${var.config["numrows"]}"
        Threads     = "${var.config["threads"]}"
        Step        = "${lookup(var.config, "step", 0)}"
        Tables      = "${var.config["tables"]}"
        Disksize    = "${var.config["disk_size"]}"
        Datadir     = "${lookup(var.config, "datadir", var.default_datadir)}"
        Resultsdir  = "${lookup(var.config, "resultsdir", var.results_dir)}" 
        Cycles      = "${lookup(var.config, "cycles", var.defcycles)}"        # Default 1 cycle 
        Duration    = "${lookup(var.config, "duration", var.defduration)}"    # Default 300 seconds, 5 mins
        Snapshottag = "${lookup(var.config, "snapshotname", "")}"             # Look for the snapshotname  
    }   
}