terraform {
  backend "s3" {
    bucket = "benchplat"
    key    = "static/main"
    region = "us-east-1"
  }
}