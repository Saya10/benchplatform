variable "project" {
    default = "benchplatform"
    description = "Project Name (used widely)"
}

variable "configfile" {
    default = "../etc/config.yaml"
} 

variable "region" {
    default = "us-east-1"
}

variable "s3pathsuffix" {
    default = "-results"
}

variable "provider" {
    default = "aws"
}

variable "bucketName" {
    default = "benchplatform.ongres.com"
}