import psycopg2
#from ipywidgets import widgets
#from ipywidgets import interactive, interact
import matplotlib.pyplot as plt 
import matplotlib as mpl
import numpy as np 
from colour import Color # rainbow , available colour.COLOR_NAME_TO_RGB
                         # https://github.com/vaab/colour/blob/master/colour.py#L52-L192
                         
# This plot mixes all types of benchs, it does not discriminate.
def plotRun(id,clr,title="Raw Plot"):
    dburl="postgresql://postgres@localhost:5432/benchplatform"
    client = psycopg2.connect(dburl)
    cur = client.cursor()
    sqlTarget = """
        select distinct target from bysecond_runs  where id = '{}';
        """.format(id)
    cur.execute(sqlTarget)
    getTarget = cur.fetchall()[0]
    sqlTrace ="""
        select row_number() OVER(), qps 
            FROM (select threads,sec,qps from bysecond_runs where id = '{}'  
            group by threads,sec,qps order by threads,sec asc) a;
        """.format(id)
    cur.execute(sqlTrace)
    getTrace = cur.fetchall()
    plt.plot([ r[0] for r in getTrace ],[ r[1] for r in getTrace ],clr, label=getTarget)
    plt.legend()
    plt.title('{}'.format(title))
    plt.ylabel("Tx/s")
    plt.xlabel("Time Secs")
    return plt
    
def plotRunByThreads(id,betweenCond,bucket=5,btype="5050",colour="green",factor="1"):
    rainbow = {}
    dburl="postgresql://postgres@localhost:5432/benchplatform"
    client = psycopg2.connect(dburl)
    cur = client.cursor()

    sqlTarget = """
        select distinct target from bysecond_runs  where id = '{}';
        """.format(id)
    cur.execute(sqlTarget)
    getTarget = cur.fetchone()[0]
    
    sqlThreads = """
        SELECT distinct threads 
        FROM bysecond_runs 
        WHERE id = '{}' 
            and threads % 50 = 0 and threads BETWEEN {} ORDER BY threads DESC;
        """
    cur.execute(sqlThreads.format(id,betweenCond))

    getThreads = cur.fetchall()
    whiter = Color('white') 
    colorpick = Color(colour)
    thlen = len(getThreads)

    rclr = list( [c.get_rgb() for c in colorpick.range_to(whiter,thlen*3) ] )

    for th,clr in zip(getThreads,rclr[:thlen]):
        rainbow[th[0]] = clr 
    
    xoffset = 0
    for thread in getThreads:   
        thd = thread[0] 
        
        # This query ignores threads that aren't module of the step variable in the benchmarks.
        # The above won't be ignored and it will be graphed in a different function, marking the
        # entries with threads lower than the expected. It won't be unexpected to see more delay on
        # setting up the connection in Postgres and Pgbouncer[1]. 
        # [1] Although the numbers supports that improves throughput thanks to less spinning times,
        # the benchmarks won't take advantage of this, as these pools are better in conditions wheter
        # connections varies constantly. Otherwise, it just behaves as connecting to database directly.
        ts_threads = """
            WITH bysecAgg AS (
                select 
                  sec,
                  qps,
                  avg(qps) OVER () as av,
                  max(qps) OVER () as mx,
                  stddev(qps::double precision) OVER () as sdev,
                  avg(qps) OVER (PARTITION BY round(sec/{})) as secBucketQpsAvg
                  -- ,(qps - avg(qps) over ())
                  -- / (stddev(qps) over ()) as zscore
                from bysecond_runs 
                where id = '{}' and threads = {} and type = '{}' and node ~ 'client.*-{}'
                group by sec,qps 
                order by sec asc
            ), bysec AS (
                select row_number() OVER () as xoffset, 
                    qps, 
                    round(av) as av,
                    mx as mx,
                    round(sdev) as sdev, 
                    round(secBucketQpsAvg) as secBucketQpsAvg
                    FROM bysecAgg
                    -- WHERE
                    -- bysecagg.zscore >= 1.645
            )
            SELECT xoffset,qps, av,mx,sdev,secBucketQpsAvg, max(xoffset) OVER ()
            FROM bysec 
            
            ;
        """.format(bucket,id,thd,btype,factor)
        cur.execute(ts_threads)
        getTrace = cur.fetchall()

        av = getTrace[0][2]
        mx  = getTrace[0][3]
        sdev = getTrace[0][4]
        target = getTarget
        #plt.subplot()
        plt.plot([ r[0]+xoffset for r in getTrace ],[ r[5] for r in getTrace ],
                 color=rainbow[thd],
                label="{}: {} - {} - {} - {}".format(target,thd,av,mx,sdev))
        xoffset = xoffset + getTrace[0][6]
        
        plt.legend()
    plt.ylabel("Q-E/s")
    plt.xlabel("Time (secs)")
    return plt

def plotRunByThreadsLatency(id,betweenCond,bucket=5,btype="5050",colour="green",factor="1"):
    rainbow = {}
    dburl="postgresql://postgres@localhost:5432/benchplatform"
    client = psycopg2.connect(dburl)
    cur = client.cursor()

    sqlTarget = """
        select distinct target from bysecond_runs  where id = '{}';
        """.format(id)
    cur.execute(sqlTarget)
    getTarget = cur.fetchone()[0]
    
    sqlThreads = """
        SELECT distinct threads 
        FROM bysecond_runs 
        WHERE id = '{}' 
            and threads % 50 = 0 and threads BETWEEN {} ORDER BY threads DESC;
        """
    cur.execute(sqlThreads.format(id,betweenCond))

    getThreads = cur.fetchall()
    whiter = Color('white') 
    colorpick = Color(colour)
    thlen = len(getThreads)

    rclr = list( [c.get_rgb() for c in colorpick.range_to(whiter,thlen*3) ] )

    for th,clr in zip(getThreads,rclr[:thlen]):
        rainbow[th[0]] = clr 
    
    xoffset = 0
    for thread in getThreads:   
        thd = thread[0] 
        
        # This query ignores threads that aren't module of the step variable in the benchmarks.
        # The above won't be ignored and it will be graphed in a different function, marking the
        # entries with threads lower than the expected. It won't be unexpected to see more delay on
        # setting up the connection in Postgres and Pgbouncer[1]. 
        # [1] Although the numbers supports that improves throughput thanks to less spinning times,
        # the benchmarks won't take advantage of this, as these pools are better in conditions wheter
        # connections varies constantly. Otherwise, it just behaves as connecting to database directly.
        ts_threads = """
            WITH bysecAgg AS (
                select 
                  sec,
                  latency_ms_95th,
                  avg(latency_ms_95th) OVER () as av,
                  max(latency_ms_95th) OVER () as mx,
                  stddev(latency_ms_95th::double precision) OVER () as sdev,
                  avg(latency_ms_95th) OVER (PARTITION BY round(sec/{})) as secBucketQpsAvg
                from bysecond_runs 
                where id = '{}' and threads = {} and type = '{}' and node ~ 'client.*-{}'
                group by sec,latency_ms_95th 
                order by sec asc
            ), bysec AS (
                select row_number() OVER () as xoffset, 
                    latency_ms_95th, 
                    round(av) as av,
                    mx as mx,
                    round(sdev) as sdev, 
                    round(secBucketQpsAvg) as secBucketQpsAvg
                    FROM bysecAgg
            )
            SELECT xoffset,latency_ms_95th, av,mx,sdev,secBucketQpsAvg, max(xoffset) OVER ()
            FROM bysec 
            
            ;
        """.format(bucket,id,thd,btype,factor)
        cur.execute(ts_threads)
        getTrace = cur.fetchall()

        av = getTrace[0][2]
        mx  = getTrace[0][3]
        sdev = getTrace[0][4]
        target = getTarget
        #plt.subplot()
        plt.plot([ r[0]+xoffset for r in getTrace ],[ r[5] for r in getTrace ],
                 color=rainbow[thd],
                label="{}: {} - {} - {} - {}".format(target,thd,av,mx,sdev))
        xoffset = xoffset + getTrace[0][6]
        
        plt.legend()
    plt.ylabel("Latency 95th")
    plt.xlabel("Time (secs)")
    return plt


def plotSarLoadAvg(id, colour="green", title="",factor="1",node="datanode",db="benchplatform"):
    dburl="postgresql://postgres@localhost:5432/{}".format(db)
    client = psycopg2.connect(dburl)
    cur = client.cursor()
    whiter = Color('white') 
    colorpick = Color(colour)
    rclr = list( [c.get_rgb() for c in colorpick.range_to(whiter,3*3) ] )
    
    sqlTarget = """
        SELECT distinct target from sar_load_avg where id = '{}';
        """
    cur.execute(sqlTarget.format(id))
    getTarget = cur.fetchone()[0]

    tsSQL = """
        SELECT  row_number() OVER (ORDER BY stamp ASC) , 
                runq_size, plist_size , load_avg_1 , load_avg_5, 
                load_avg_15 , blocked 
        FROM sar_load_avg
        WHERE id = '{}' 
              and node ~ '{}.*{}$'
              -- This wont work on factor =2, or t
        ORDER BY stamp ASC
    """.format(id,node,factor) 
    cur.execute(tsSQL)   
    getTS     = cur.fetchall()

    #min1avg = [ np.mean(r[3]) for r in getTS ]
    #min5avg = [ np.mean(r[4]) for r in getTS ]
    plt.plot([ r[0] for r in getTS ],[ r[3] for r in getTS ], color=rclr[0],label="{} - 1 min".format(getTarget) )
    plt.plot([ r[0] for r in getTS ],[ r[4] for r in getTS ], color=rclr[2],label="{} - 5 min".format(getTarget) )
    #plt.plot([ r[0] for r in getTS ],[ r[5] for r in getTS ], color=rclr[4],label="15 min " )

    plt.legend()
    #plt.title("Load Avg - {}".format(getTarget))
    plt.title(title)
    plt.ylabel("Load Avg")
    plt.xlabel("Time")
    return plt 


def plotSarCS(id, colour="green",factor="1",node="datanode",db="benchplatform"):
    dburl="postgresql://postgres@localhost:5432/{}".format(db)
    client = psycopg2.connect(dburl)
    cur = client.cursor()

    sqlTarget = """
        SELECT distinct target from sar_context_switches where id = '{}';
        """
    cur.execute(sqlTarget.format(id))
    getTarget = cur.fetchone()[0]
    tsSQL = """
        SELECT  stamp , processes_per_second , cs_per_second
        FROM sar_context_switches
        WHERE id = '{}'
            and node ~ '{}.*{}$'
        ORDER BY stamp ASC
    """.format(id,node,factor)   
    cur.execute(tsSQL) 
    getTS     = cur.fetchall()

    plt.plot([ r[0] for r in getTS ],[ r[1] for r in getTS ], color=colour,label="{}".format(getTarget) )
    plt.legend()
    plt.ylabel("CS")
    plt.xlabel("Time")
    return plt 