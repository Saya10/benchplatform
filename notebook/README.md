# IPython Notebook or Jupyter Notebook 

## Sources

- https://ipython.org/notebook.html
- https://ipython.readthedocs.io/en/stable/
- https://ipython.readthedocs.io/en/stable/install/index.html

## How To install IPython Notebooks on linux (Ubuntu 18.04 x64).

> Note: In the main README there is instructions about install packages into requirements.txt, but psycopg2 fails. 
> So, install the psycopg2 with apt tool as follows

```
sudo apt-get install python3-psycopg2
sudo apt-get install libpq-dev
sudo apt-get install python3-pip
pip3 install ipython
pip3 install pyyaml
pip3 install yq
pip3 install awscli
pip3 install jupyter
pip3 install nbconvert
pip3 install matplotlib
pip3 install numpy
pip3 install ipywidgets
pip3 install ipython-sql
pip3 install colour
pip3 install pandas
python3 -m pip install ipykernel
```
