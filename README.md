# benchPlatform

> NOTE: Not public, deprecated for new features as it is going to be rewritten from scratch.

This project sets a cloud-philosophy becnhmark framework, for coding parallelized, scaled and fair comparisions 
between Database engines in general.

This particular one, includes three types of benchmark: PostgreSQL, PostgreSQL+PgBouncer and MongoDB.

## Parts

- AWS was used by requirement, but it is intended to keep agnostic the flow, for migrating to other providers.
- Images (Packer)
- make initialLoad (Terraform + Snapshot)
- make benchmarks (Terraform)

## Requirements


- Python3 is required for analizing data through iPython.
- Please install the  https://github.com/ashald/terraform-provider-yaml plugin in the folder. This is an external Data Source
- https://yq.readthedocs.io/en/latest/ and `jq` are widely used in bin/ tools


## Howto start

First, you need a `.env` file with the following completed credentials and variables:

```
export AWS_ACCESS_KEY_ID=...
export AWS_SECRET_ACCESS_KEY=...
export AWS_DEFAULT_REGION="us-east-1"
```

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```


### Building images

The first thing to execute before anything, is to create the images. There has been included three images,
which contain a Postgres, Mongo and a generic client for both engines .

```
make TARGET=all image
```

Or, enter the `packer` folder and execute `make all` or for  building specific images: `make postgres-build` (see under `packer/src/<dbengine>.json`)


## Building base

This step should be executed _once_:

```
make setup
make init
make plan
make apply
```

> `make setup` creates the S3 bucket for the Terraform resources and attach a passwordless temporal SSH key as a key pair.


The above will create the base architecture (networks, keypair, etc).

You can use custom tfvars by passing `DEFVARS="<path>"` to make command.
We suggest to use `terraform/etc/` folder.

## Coding modules

When you code modules, you need to also add their corresponding terraform template under
`terraform/templates/<module_name>.template` which will be used in the terraform/ directory
to call your module with the etc/config.yaml variables provided.

## Actions

Actions are something that you can actually add into the modules you code under `modules/`. The 
action is the `make` _dynamic target_ (see Makefile, under `%:`).

So far, we have `load` and `run`, but if you create all the corresponding folders
and scripts, you can create your own.

Even tho, the current doc will focus on the available.


## Configuration

Configuration is placed in `etc/config.yaml` wether either all snapshots and benchmark are defined.
The variables aren't fixed, meaning that your modules can have different variables.


## Standard module

### Building Data Snapshots

As this project intends to parallelize the benchmarks, we build the datasets and snapshot
the storages for being attach in the following phase.

```
make TARGET="snapshot-postgres-sysbench-xfs-fit" prepare
make TARGET="module.snapshot-postgres-sysbench-xfs-fit" plan
make TARGET="module.snapshot-postgres-sysbench-xfs-fit" apply
```

The TARGET should be an entry on `/etc/config.yaml`. A basic entry should look like:

```
snapshot-postgres-sysbench-xfs-fit:
  action: load
  dbengine: "postgres"
  tool: "sysbench"
  fs: "xfs"
  loadsize: "fit"
  factor: 1
  # Use the corresponding size! Check README 
  disk_size: 200

  datadir: "/opt/data"
  device_name: "/dev/xvdb"
  ebs_type: "io1"
  disk_iops: "1000" 
  datanode_instance_type: "t2.small"
  client_instance_type: "t2.small"

  # Image
  datanode_image_name: "benchplatform-postgres"
  client_image_name: "benchplatform-client"

  # Trigger script
  trigger_script: "postgres-sysbench"
  numrows: 1000000
  threads: 25
  tables: 10
  dbname: "benchplatform"
  ```

> The snapshot name will be the TARGET.

Locally, you can get the status of the snapshots:

```
source .env && python /usr/local/bin/aws --region="us-east-1" ec2 describe-snapshots --filter Name=tag:Name,Values=snapshot-postgres-sysbench-xfs-fit | jq '.Snapshots[].State' -r
```

For getting the status of the run, you can check the message queue:

```
aws --region="us-east-1"  sqs receive-message --queue-url  https://queue.amazonaws.com/835803195223/benchplatform-status-queue.fifo --attribute-name MessageGroupId --max-number-of-messages 10 | jq '.Messages[] | select(.Attributes.MessageGroupId | contains("snapshot-postgres-sysbench-xfs-fit")) | .Body'
```

Change the MessageGroupId with the target name.

Or, use the bin tool:

```
~benchplatform $ bin/runningSnapshots
snapshot-postgres-github-xfs 20190412210615283333373 - Snapshot Status: pending
snapshot-postgres-github-xfs 20190412210615283333373 - Snapshot Status: completed
```



### Running benchmarks

```
make TARGET="postgres-sysbench-xfs-fit" plan
make TARGET="postgres-sysbench-xfs-fit" apply
```


### Debugging startup

All startup is done through cloud-init provisioning, which can be visualized from the client node
within a simple look:

```
tail -f /var/log/cloud-init-output.log
```

Or, use `bin/status <ip>`, which will execute the same as above.

## bin/ tools

- bin/jump <ip>
  - Used for jumping to an host 
- bin/getResults <path S3>
  - Use this to download S3 results from a relative path (target/uuid or just target)
- bin/listConfigs
  - List configs in the yaml, with a better formatting
- bin/checkSnapshots <target>
  - Show you the status of the snapshots for the given target.
- bin/runningSnapshots <target>
  - Shows the queue messages about the snapshots being run

## Analizing data

### Requirements

For analizing data, you need notebook running. Even tho this is not strictly part of the 
framework project, we strongly recommend to use this.

Also, you need a Postgres endpoint for uploading files.



