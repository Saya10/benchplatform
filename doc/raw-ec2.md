
## Setup a EC2 + volume with registry module

https://github.com/terraform-aws-modules/terraform-aws-ec2-instance/edit/master/examples/volume-attachment/main.tf

```
module "ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"
  version = "1.19.0"
  instance_count = 1

  name                        = "datanode"
  ami                         = "${data.aws_ami.benchplatform_<db>.id}"
  instance_type               = "m4.large"
  subnet_id                   = "${element(data.aws_subnet_ids.all.ids, 0)}"
  vpc_security_group_ids      = ["${module.security_group.this_security_group_id}"]
  associate_public_ip_address = true
 tags = {
    Terraform = "true"
    Environment = "dev"
  }
  # user_data = ""
  # iam_instance_profile = "aws_iam_policy" "ins-user-pol"
}

resource "aws_volume_attachment" "this_ec2" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.this.id}"
  instance_id = "${module.ec2.id[0]}"
}

resource "aws_ebs_volume" "this" {
  availability_zone = "${module.ec2.availability_zone[0]}"
  size              = 1
}


/////

output "instance_id" {
  description = "EC2 instance ID"
  value       = "${module.ec2.id[0]}"
}

output "instance_public_dns" {
  description = "Public DNS name assigned to the EC2 instance"
  value       = "${module.ec2.public_dns[0]}"
}

output "ebs_volume_attachment_id" {
  description = "The volume ID"
  value       = "${aws_volume_attachment.this_ec2.volume_id}"
}

output "ebs_volume_attachment_instance_id" {
  description = "The instance ID"
  value       = "${aws_volume_attachment.this_ec2.instance_id}"
}

```



Testing YAML: `terraform apply -var-file=tfvars/template.tfvars -target="data.yaml_map_of_strings.snapconf"`



```
python /usr/local/bin/aws ec2 describe-instances --filters Name=instance-state-name,Values=running | jq
python /usr/local/bin/aws ec2 terminate-instan
ces --instance-ids i-0f6428048c4c4b6be

python /usr/local/bin/aws iam remove-role-from-instance-profile --instance-profile-name generic-profile --role-name generic-role

python /usr/local/bin/aws iam delete-instance-profile --instance-profile-name generic-profile
python /usr/local/bin/aws iam delete-role --role-name generic-role
python /usr/local/bin/aws ec2 delete-key-pair  --key-name benchplatform
```


To get tags from the instance:

```
ec2-describe-tags --filter "resource-type=instance" --filter "resource-id=$(getAttrMetadata instance-id)" --filter "key=Fs" | cut -f5
```
